#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <net/bpf.h>

#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <netinet/if_ether.h>
#include <arpa/inet.h>
#include <net/if.h>
#include <net/ethernet.h>

#define IP4_HLEN 20
#define UDP_HLEN 8
#define ETH_HLEN 14

unsigned short int checksum(unsigned short int *, int);
unsigned short int udp4_checksum(struct ip, struct udphdr,
    unsigned char *, int);


  int
main(int argc, char *argv[])
{

  unsigned char  *ether_frame, *data;
  char *src_ip, *dst_ip;
  int datalen, framelen, bc;
  struct ether_header eh;
  struct ip iph;
  struct udphdr udph;

  data = (unsigned char*)malloc((IP_MAXPACKET - IP4_HLEN - UDP_HLEN) *
      sizeof(unsigned char));
  memset(data, 0, (IP_MAXPACKET - IP4_HLEN - UDP_HLEN));

  ether_frame = (unsigned char*)malloc(IP_MAXPACKET *
      sizeof(unsigned char));
  memset(ether_frame, 0, IP_MAXPACKET * sizeof(unsigned char));

  src_ip = (char*)malloc(16 * sizeof(char));
  memset(src_ip, 0, 16 * sizeof(char));

  dst_ip = (char*)malloc(16 * sizeof(char));
  memset(src_ip, 0, 16 * sizeof(char));

  eh.ether_dhost[0] = 0x00;
  eh.ether_dhost[1] = 0x25;
  eh.ether_dhost[2] = 0x64;
  eh.ether_dhost[3] = 0x63;
  eh.ether_dhost[4] = 0x59;
  eh.ether_dhost[5] = 0x14;

  eh.ether_shost[0] = 0x08;
  eh.ether_shost[1] = 0x00;
  eh.ether_shost[2] = 0x27;
  eh.ether_shost[3] = 0x1a;
  eh.ether_shost[4] = 0x1b;
  eh.ether_shost[5] = 0x97;

  eh.ether_type = 0x0800;

  strcpy(src_ip, "193.178.153.136");
  strcpy(dst_ip, "193.178.153.123");

  data[0] = 'b';
  data[1] = 'i';
  data[2] = 'j';
  data[3] = 'e';
  data[4] = 'v';

  datalen = 6;

  iph.ip_hl = IP4_HLEN / (sizeof(unsigned long int));
  iph.ip_v = 4;
  iph.ip_tos = 0;
  iph.ip_len = htons(IP4_HLEN + UDP_HLEN + datalen);
  iph.ip_id = htons(0);
  iph.ip_off = 0;
  iph.ip_ttl = 255;
  iph.ip_p = IPPROTO_UDP;
  iph.ip_src.s_addr = inet_network(src_ip);
  iph.ip_dst.s_addr = inet_network(dst_ip);
  iph.ip_sum = 0;
  iph.ip_sum = checksum((unsigned short int*)&iph, IP4_HLEN);

  udph.uh_sport = htons(8887);
  udph.uh_dport = htons(8887);
  udph.uh_ulen = htons(UDP_HLEN + datalen);
  udph.uh_sum = udp4_checksum(iph, udph, data, datalen);

  framelen = ETHER_ADDR_LEN + ETHER_ADDR_LEN + ETHER_TYPE_LEN +
    IP4_HLEN + UDP_HLEN + datalen;

  memcpy(ether_frame, &eh, ETH_HLEN);
  memcpy(ether_frame + ETH_HLEN, &iph, IP4_HLEN);
  memcpy(ether_frame + ETH_HLEN + IP4_HLEN, &udph, UDP_HLEN);
  memcpy(ether_frame + ETH_HLEN + IP4_HLEN + UDP_HLEN, data, datalen);


  char *dev_name;
  dev_name = (char*)malloc(strlen("/dev/bpf" + 1));
  memset(dev_name, 0, sizeof(dev_name));
  int bpf_desc;
  int i;
  for (i = 0; i < 99; i++){
    sprintf(dev_name, "/dev/bpf%d", i);
    bpf_desc = open(dev_name, O_RDWR);
    if (bpf_desc != 0)
      break;
  }

  char *interface = "em0";
  struct ifreq bound_if;
  strcpy(bound_if.ifr_name, interface);
  if (ioctl( bpf_desc, BIOCSETIF, &bound_if) < 0){
    perror("BIOCSETIF failed");
    exit(1);
  }

  int buf_len = 1;
  if (ioctl(bpf_desc, BIOCIMMEDIATE, &buf_len) < 0){
    perror("BIOCIMMEDIATE failed");
    exit(1);
  }

  if (ioctl(bpf_desc, BIOCGBLEN, &buf_len) < 0){
    perror("BIOCGBLEN failed");
    exit(1);
  }

  struct bpf_hdr *bpf_buf = (struct bpf_hdr*)malloc(sizeof(buf_len));
  memset(bpf_buf, 0, sizeof(buf_len));
  struct bpf_hdr *bpf_packet;

  if ((bc = write(bpf_desc, ether_frame, framelen)) < 0){
    perror("write failed");
    exit(1);
  }
  return EXIT_SUCCESS;
}

  unsigned short int
checksum(unsigned short int *addr,
    int len)
{
  int nleft = len;
  int sum = 0;
  unsigned short *w = addr;
  unsigned short int answer = 0;

  while (nleft > 1) {
    sum += *w++;
    nleft -= sizeof(unsigned short int);
  }

  if (nleft == 1){
    *(unsigned char *)(&answer) = *(unsigned char *)w;
    sum += answer;
  }

  sum = (sum >> 16) + (sum & 0xFFFF);
  sum += (sum >> 16);
  answer = ~sum;
  return answer;
}

  unsigned short int
udp4_checksum(struct ip iph, struct udphdr udph, unsigned char *payload,
    int payloadlen)
{
  char buff[IP_MAXPACKET];
  char *ptr;
  int chksumlen = 0;
  int i;

  ptr = &buff[0];

  memcpy(ptr, &iph.ip_src.s_addr,
      sizeof(iph.ip_src.s_addr));
  ptr += sizeof(iph.ip_src.s_addr);
  chksumlen += sizeof(iph.ip_src.s_addr);

  memcpy(ptr, &iph.ip_dst.s_addr,
      sizeof(iph.ip_dst.s_addr));
  ptr += sizeof(iph.ip_dst.s_addr);

  *ptr = 0;
  ptr++;
  chksumlen += 1;

  memcpy(ptr, &iph.ip_p, sizeof(iph.ip_p));
  ptr += sizeof(iph.ip_p);
  chksumlen += sizeof(iph.ip_p);

  memcpy(ptr, &udph.uh_ulen, sizeof(udph.uh_ulen));
  ptr += sizeof(udph.uh_ulen);
  chksumlen += sizeof(udph.uh_ulen);

  memcpy(ptr, &udph.uh_sport, sizeof(udph.uh_sport));
  ptr += sizeof(udph.uh_sport);
  chksumlen += sizeof(udph.uh_sport);

  memcpy(ptr, &udph.uh_dport, sizeof(udph.uh_dport));
  ptr += sizeof(udph.uh_dport);
  chksumlen += sizeof(udph.uh_dport);

  memcpy(ptr, &udph.uh_ulen, sizeof(udph.uh_ulen));
  ptr += sizeof(udph.uh_ulen);
  chksumlen += sizeof(udph.uh_ulen);

  *ptr = 0;
  ptr++;
  *ptr = 0;
  ptr++;
  chksumlen += 2;

  memcpy(ptr, payload, payloadlen);
  ptr += payloadlen;
  chksumlen += payloadlen;

  for (i = 0; i < payloadlen % 2; i++, ptr++){
    *ptr = 0;
    ptr++;
    chksumlen++;
  }
  return checksum((unsigned short int *)buff, chksumlen);
}
