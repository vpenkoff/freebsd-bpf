#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argvp[]) {
  unsigned int x;
  printf("Enter value: \n");
  scanf("%x", &x);

  printf("INT: %u\n", x);
  printf("HEX: %x\n", x);
  return EXIT_SUCCESS;
}
