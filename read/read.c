#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/time.h>
#include <sys/ioctl.h>
#include <net/bpf.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <netinet/if_ether.h>
#include <net/if.h>
#include <netinet/udp.h>
#include <net/ethernet.h>


struct ether_frame{
  struct ether_header eh;
  struct ip iph;
}__packed;

  int
main(int argc, char *argv[])
{
  char *dev_name;
  dev_name = (char*)malloc(strlen("/dev/bpf" + 1));
  memset(dev_name, 0, sizeof(dev_name));
  int buf_var = 1;
  int buf_len;
  int bpf_desc;
  int i;
  for (i = 0; i < 99; i++){
    sprintf(dev_name, "/dev/bpf%d", i);
    bpf_desc = open(dev_name, O_RDWR);
    if (bpf_desc != 0)
      break;
  }

  int bufsize = BPF_MAXBUFSIZE;

  if (ioctl(bpf_desc, BIOCSBLEN, &bufsize) < 0){
    perror("BIOCSBLEN failed");
    exit(1);
  }

  if (ioctl(bpf_desc, BIOCSETBUFMODE, &buf_var) < 0){
    perror("BIOCSETBUFMODE failed");
    exit(1);
  }

  if (ioctl(bpf_desc, BIOCFLUSH) < 0){
    perror("BIOCFLUSH failed");
    exit(1);
  }

  char *interface = "em0";
  struct ifreq bound_if;

  strcpy(bound_if.ifr_name, interface);

  if (ioctl( bpf_desc, BIOCSETIF, &bound_if) < 0){
    perror("BIOCSETIF failed");
    exit(1);
  }



  if (ioctl(bpf_desc, BIOCIMMEDIATE, &buf_var) < 0){
    perror("BIOCIMMEDIATE failed");
    exit(1);
  }

  if (ioctl(bpf_desc, BIOCGBLEN, &buf_len) < 0){
    perror("BIOCGBLEN failed");
    exit(1);
  }
  printf("Buffer size: %d\n", buf_len);

  int bc = 0;
  struct ether_frame *ef;
  struct bpf_hdr *bpf_buf = (struct bpf_hdr*)malloc(sizeof(buf_len));
  memset(bpf_buf, 0, sizeof(buf_len));
  struct bpf_hdr *bpf_packet;

  struct bpf_insn insns[] = {
    BPF_STMT(BPF_LD+BPF_H+BPF_ABS, 12),
    BPF_JUMP(BPF_JMP+BPF_JEQ+BPF_K, ETHERTYPE_IP, 0, 7 ),
    BPF_STMT(BPF_LD+BPF_B+BPF_ABS, 23),
    BPF_JUMP(BPF_JMP+BPF_JEQ+BPF_K, IPPROTO_ICMP, 0, 5),
    BPF_STMT(BPF_LD+BPF_W+BPF_ABS, 30),
    BPF_JUMP(BPF_JMP+BPF_JEQ+BPF_K, 0x0a00020f, 0, 3),
    BPF_STMT(BPF_LD+BPF_W+BPF_ABS, 26),
    BPF_JUMP(BPF_JMP+BPF_JEQ+BPF_K, 0xc2913f0c, 0, 1),
    BPF_STMT(BPF_RET+BPF_K, (u_int)-1),
    BPF_STMT(BPF_RET+BPF_K, 0)
  };

  struct bpf_program bpf_read;
  bpf_read.bf_len = sizeof(insns) / sizeof(struct bpf_insn);
  bpf_read.bf_insns = insns;

  if (ioctl(bpf_desc, BIOCSETFNR, &bpf_read) < 0){
    perror("BIOCSETF failed");
    exit(1);
  }

  int j;
  int k;
  int recv=0;
  int sum=0;
  for(j = 0; j < 100; j++){

    if ((bc = read(bpf_desc, bpf_buf, buf_len)) < 0){
      perror("read failed");
      exit(1);
    }
    else {
      char *ptr = (char*)bpf_buf;
      while( ptr < (char*)bpf_buf + bc){
        bpf_packet = (struct bpf_hdr*)ptr;
        ef = (struct ether_frame*)((char*)bpf_packet +
            bpf_packet->bh_hdrlen);
        printf("HEX:\t");
        for (k = 0; k < sizeof(ptr); k++){
          printf("%02x", ef[k]);
        }
        printf("\n");
        printf("ether_dhost: %s\n",
            ether_ntoa(ef->eh.ether_dhost));
        printf("ether_shost: %s\n",
            ether_ntoa(ef->eh.ether_shost));
        printf("ether_type: %02x\n",
            ef->eh.ether_type);
        printf("ip_src: %s\n",
            inet_ntoa(ef->iph.ip_src));
        printf("Ip_dst: %s\n",
            inet_ntoa(ef->iph.ip_dst));
        printf("ip_proto: %d\n",
            (unsigned int)ef->iph.ip_p);
        printf("Captured length: %d\n",
            (bpf_packet->bh_caplen)/sizeof(char));
        printf("\n");
        ptr += BPF_WORDALIGN(bpf_packet->bh_hdrlen +
            bpf_packet->bh_caplen);
      }
      recv++;
      sum += bpf_packet->bh_caplen;
    }

  }
  /*
     struct bpf_stat stats;
     if (ioctl(bpf_desc, BIOCGSTATS, &stats) < 0){
     perror("BIOCGSTATS failed");
     exit(1);
     }

     printf("RECV packets: %d\n", stats.bs_recv);
   */
  printf("Packets: %d\n", recv);
  printf("All captured len: %d\n", sum/sizeof(char));
  if (ioctl(bpf_desc, BIOCFLUSH) < 0){
    perror("BIOCFLUSH failed");
    exit(1);
  }

  free(dev_name);
  free(bpf_buf);
  return EXIT_SUCCESS;
}
